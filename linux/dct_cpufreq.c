/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#include <linux/clk.h>
#include <linux/cpu.h>
#include <linux/cpufreq.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/pm_opp.h>

#define DCT_CPU_FREQ_INTERVAL 100000000

static struct platform_device *cpufreq_dt;

static int dct_cpufreq_probe(struct platform_device *pdev)
{
	struct clk *clk;
	struct device *cpu_dev;
	unsigned long rate;
	unsigned long max;
	unsigned long min;
	unsigned long last_opp_rate = 0;
	int ret;

	cpu_dev = get_cpu_device(0);
	if (!cpu_dev) {
		pr_err("Cannot get CPU 0 for cpufreq driver\n");
		return -ENODEV;
	}

	clk = clk_get(cpu_dev, NULL);
	if (IS_ERR(clk)) {
		dev_err(cpu_dev, "Cannot get clock for CPU 0\n");
		return PTR_ERR(clk);
	}

	min = roundup(clk_round_rate(clk, 0), DCT_CPU_FREQ_INTERVAL);
	max = roundup(clk_round_rate(clk, ULONG_MAX), DCT_CPU_FREQ_INTERVAL);
	pr_debug("CPU reports supported clock range: min - max: %lu - %lu\n", min, max);

	for (rate = min; rate <= max; rate += DCT_CPU_FREQ_INTERVAL) {
		unsigned long rounded_rate = clk_round_rate(clk, rate);
		if (rounded_rate == 0) {
			pr_err("clk_round_rate returned 0 for requested rate %lu\n", rate);
			goto remove_opp;
		}
		if (rounded_rate != last_opp_rate) {
			ret = dev_pm_opp_add(cpu_dev, rounded_rate, 0);
			if (ret) {
				goto remove_opp;
			}
			last_opp_rate = rounded_rate;
		}
	}
	clk_put(clk);

	cpufreq_dt = platform_device_register_simple("cpufreq-dt", -1, NULL, 0);
	ret = PTR_ERR_OR_ZERO(cpufreq_dt);
	if (ret) {
		pr_err("Failed to create platform device, %d\n", ret);
		goto remove_opp;
	}

	return 0;

remove_opp:
	dev_pm_opp_remove_all_dynamic(cpu_dev);

	return ret;
}

static int dct_cpufreq_remove(struct platform_device *pdev)
{
	struct device *cpu_dev;

	cpu_dev = get_cpu_device(0);
	if (cpu_dev) {
		dev_pm_opp_remove_all_dynamic(cpu_dev);
	}

	platform_device_unregister(cpufreq_dt);

	return 0;
}

/*
 * Since the driver depends on clk-dct, which may return EPROBE_DEFER,
 * all the activity is performed in probe, which may be defered as well.
 */
static struct platform_driver dct_cpufreq_driver = {
	.driver = {
		.name = "dct-cpufreq",
	},
	.probe = dct_cpufreq_probe,
	.remove = dct_cpufreq_remove,
};
module_platform_driver(dct_cpufreq_driver);

MODULE_DESCRIPTION("DCT cpufreq driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:dct-cpufreq");
